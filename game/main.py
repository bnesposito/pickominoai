import resources as rs
import operations as op
import decisions as ds
import logging


def main():
    logging.basicConfig(format='%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt='%Y-%m-%d:%H:%M:%S',
                        level=logging.DEBUG)

    n_players = 2
    n_dice = 8

    player_a = rs.Player('A',
                         ds.pick_highest_points_dice,
                         ds.fix_illegal_dice_addition,
                         ds.player_stops_playing_at_random)
    player_b = rs.Player('B',
                         ds.pick_dice_at_random,
                         ds.fix_illegal_dice_addition,
                         ds.player_stops_playing_at_random)

    player_list = [player_a, player_b]

    # Set up new game
    point_holder_board = op.set_up_board_point_holder()
    point_holder_player_list = op.set_up_players_point_holder(n_players)
    selected_dice_holder = rs.DiceHolderPlayer()

    flag_end_game = 0
    round_cont = 1

    while flag_end_game == 0:
        for temp_player in range(1, n_players+1):
            player_active = player_list[temp_player-1]
            selected_dice_holder.reset()
            dice_holder = op.set_up_dice_holder(n_dice)
            flag_stop_player_turn = 0
            flag_player_lost_turn = 0

            while flag_stop_player_turn == 0:

                dice_holder.roll()
                logging.info('Round {} - Turn Player {} - New roll: {}'.format(round_cont, temp_player, dice_holder))
                grouped_dice = dice_holder.group()
                temp_selected_dice_index = player_active.select_dice(grouped_dice)
                temp_selected_dice_array = grouped_dice[temp_selected_dice_index]

                # Check if the dice selection is legal and can be added
                flag_legal_add = selected_dice_holder.check_legal_add(temp_selected_dice_array)
                # Try yo fix illegal dice addition
                if flag_legal_add == 0:
                    (temp_selected_dice_array, flag_legal_add) = player_active.fix_dice_addition(selected_dice_holder,
                                                                                                 grouped_dice)
                # No possible dice addition.
                if flag_legal_add == 0:
                    logging.warning('End of round: There are no legal dice addition for Player {}'.format(temp_player))
                    flag_stop_player_turn = 1
                    flag_player_lost_turn = 1
                # There is a possible dice addition
                else:
                    selected_dice_holder.add_dice(temp_selected_dice_array)
                    logging.warning('Legal add done')
                    logging.info('Player {} dice holder value: {} - {}'.format(temp_player,
                                                                               selected_dice_holder.value(),
                                                                               selected_dice_holder))

                    dice_holder.remove_dice(temp_selected_dice_array[1])

                # TODO: we could remove the criteria check for player stop playing. Ideally, the AI should learn it.
                # TODO: still, I am not sure if removing it is a good idea.
                # Check if player can stop playing
                worm_stop_criteria = selected_dice_holder.check_legal_stop_worm_criteria()
                token_stop_criteria = point_holder_board.check_legal_stop_token_criteria(selected_dice_holder.value())
                logging.info('Stop turn - Worm criteria: {}'.format(worm_stop_criteria))
                logging.info('Stop turn - Point token criteria: {}'.format(token_stop_criteria))

                if worm_stop_criteria == 1 and token_stop_criteria == 1:
                    logging.info('Player {} can decide to continue playing or quit'.format(temp_player))
                    # Player decides to stop playing or not
                    if player_active.stop_playing():
                        logging.warning('Round {} - Player {} stops playing'.format(round_cont, temp_player))
                        flag_stop_player_turn = 1
                    else:
                        logging.warning('Round {} - Player {} continues playing'.format(round_cont, temp_player))

                # End round if dice holder is empty
                if len(dice_holder) == 0:
                    logging.warning('End of round {}: Dice holder is empty'.format(round_cont))
                    flag_stop_player_turn = 1

            # Player does not lose turn and gets a token
            if flag_player_lost_turn == 0:
                steal_options = op.get_possible_steal_tokens(point_holder_player_list, temp_player)
                # Player steals
                if rs.PointToken(selected_dice_holder.value()) in steal_options:
                    victim_player = steal_options.index(rs.PointToken(selected_dice_holder.value()))
                    logging.warning('Player {} steals token from Player {}'.format(temp_player, victim_player+1))
                    stolen_token = point_holder_player_list[victim_player].pop()
                    point_holder_player_list[temp_player-1].append(stolen_token)
                # Player does not steal and gets a token from board
                else:
                    logging.info('Player {} cannot steal any token'.format(temp_player))
                    logging.debug('Dice holder value {}'.format(selected_dice_holder.value()))
                    logging.debug('Board {}'.format(point_holder_board))
                    (flag_value_lower_than_board,
                     selected_point_token) = ds.pick_highest_points_token(selected_dice_holder.value(),
                                                                          point_holder_board)
                    # Check whether there is a token available
                    if flag_value_lower_than_board == 1:
                        flag_player_lost_turn = 1
                    else:
                        logging.info('Highest token on board selected by Player {}: {}'.format(temp_player,
                                                                                               selected_point_token))
                        point_holder_board.remove(selected_point_token)
                        point_holder_player_list[temp_player-1].append(selected_point_token)

            # Player looses turn
            if flag_player_lost_turn == 1:
                logging.warning('Player {} lost turn'.format(temp_player))
                # Player cannot lose a token
                if len(point_holder_player_list[temp_player-1]) == 0:
                    logging.info('Player {} cannot lose token'.format(temp_player))
                # Player loses a token
                else:
                    removed_point_token = point_holder_player_list[temp_player-1].pop()
                    logging.info('Player {} lost token {}'.format(temp_player, removed_point_token))
                    point_holder_board.add(removed_point_token)
                point_holder_board.remove_highest()

            logging.warning('End of round: {}'.format(point_holder_board))
            for i in range(1, n_players+1):
                logging.warning('End of round {} - Player {} point tokens: {}'.format(round_cont,
                                                                                      i,
                                                                                      point_holder_player_list[i - 1]))

            # Condition to finish the game
            if len(point_holder_board) == 0:
                flag_end_game = 1
                break

        round_cont += 1

    logging.warning('End of game')
    for i in range(1, n_players + 1):
        logging.warning('End of game - Player {} points: {}'.format(i, point_holder_player_list[i - 1].get_points()))

    winner = op.get_winner(point_holder_player_list)
    logging.warning('Player {} won!'.format(winner))

    # TODO: get stats: number of rounds, busts, steals, bad decisions


if __name__ == '__main__':
    main()
