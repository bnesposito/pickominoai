import numpy as np
import random
from itertools import groupby, tee
import logging


class Dice:
    def __init__(self, name):
        self.name = name
        self.value = 0
        self.numeric_value = 0
        self.point_value = 0

    def __repr__(self):
        return "{}:{}".format(self.name, self.value)

    def __str__(self):
        return "{}:{}".format(self.name, self.value)

    def __hash__(self):
        return self.name

    def __eq__(self, other):
        return self.numeric_value == other.numeric_value

    def roll(self):
        self.numeric_value = random.randint(1, 6)
        if self.numeric_value == 6:
            self.value = 'worm'
            self.point_value = 5
        else:
            self.value = self.numeric_value
            self.point_value = self.numeric_value


class DiceHolder(set):
    def __init__(self):
        super(set, self).__init__()

    def roll(self):
        for i in self:
            i.roll()

    def group(self):
        sorted_dice = sorted(list(self), key=lambda x: x.numeric_value)
        grouped_dice = []
        for i, j in groupby(sorted_dice, lambda x: x.numeric_value):
            j1, j2, j3 = tee(j, 3)
            point_value = list(j3)[0].point_value
            grouped_dice.append([i, list(j1), point_value*len(list(j2))])
        logging.info('Grouped dice: {}'.format(grouped_dice))
        return grouped_dice

    def remove_dice(self, selected_dice):
        for i in selected_dice:
            self.remove(i)


class DiceHolderPlayer(set):
    def reset(self):
        self.clear()

    def get_dice_value_in(self):
        return [i.numeric_value for i in self]

    def check_legal_add(self, selected_dice_array):
        dice_already_in = self.get_dice_value_in()
        if selected_dice_array[0] not in dice_already_in:
            return 1
        else:
            logging.warning('Dice add move is not legal for array {}'.format(selected_dice_array))
            return 0

    def add_dice(self, selected_dice_array):
        for i in selected_dice_array[1]:
            self.add(i)

    def value(self):
        return sum([i.point_value for i in self])

    def check_legal_stop_worm_criteria(self):
        dice_already_in = self.get_dice_value_in()
        if 6 in dice_already_in:
            return 1
        else:
            return 0


class PointToken:
    def __init__(self, size):
        self.size = size

    def __repr__(self):
        return "{}|{}".format(self.size, self.get_points())

    def __str__(self):
        return "{}|{}".format(self.size, self.get_points())

    def __hash__(self):
        return self.size

    def __eq__(self, other):
        return self.size == other.size

    def __gt__(self, other):
        return self.size > other.size

    def __ge__(self, other):
        return self.size >= other.size

    def get_points(self):
        return np.floor((self.size - 17) / 4)


class PointHolderPlayer(list):
    def __init__(self, name):
        self.name = name
        super(list, self).__init__()

    def last_token(self):
        if len(self) != 0:
            return self[-1]
        else:
            return PointToken(0)

    def get_points(self):
        return sum([x.get_points() for x in self])

    def get_highest(self):
        return max(self)


class PointHolderBoard(set):
    def __init__(self, name):
        self.name = name
        super(set, self).__init__()

    def remove_highest(self):
        highest_token = max(self)
        logging.info('Highest token {} in board is removed'.format(highest_token))
        self.remove(highest_token)

    def check_legal_stop_token_criteria(self, selected_dice_value):
        lowest_token = min(self)
        if selected_dice_value >= lowest_token.size:
            return 1
        else:
            return 0


class Player:
    def __init__(self, name, dice_selection_function, fix_dice_addition_function, stop_playing_function):
        self.name = name
        self.dice_selection_function = dice_selection_function
        self.stop_playing_function = stop_playing_function
        self.fix_dice_addition_function = fix_dice_addition_function

    def select_dice(self, grouped_dice):
        return self.dice_selection_function(grouped_dice)

    def stop_playing(self):
        return self.stop_playing_function()

    def fix_dice_addition(self, selected_dice_holder, grouped_dice):
        return self.fix_dice_addition_function(selected_dice_holder, grouped_dice)

    # TODO: we will have to define the input for each of these decisions in a more general way.
