import resources as rs
import numpy as np
import logging


def set_up_board_point_holder():
    # Initialize the board
    point_holder_board = rs.PointHolderBoard('Board')
    for i in range(16):
        # Load instances of PointTokens into the point_holder_board
        point_holder_board.add(rs.PointToken(21 + i))
    logging.info('Set up - Point holder board: {}'.format(point_holder_board))
    return point_holder_board


def set_up_players_point_holder(n_players):
    point_holder_player_list = []
    for i in range(n_players):
        # Load instances of PointHolderPlayer into a list
        # There is one PointHolderPlayer per player
        point_holder_player_list.append(rs.PointHolderPlayer(i))
    logging.info('Set up - Point holder player list: {}'.format(point_holder_player_list))
    return point_holder_player_list


def set_up_dice_holder(n_dice):
    dice_holder = rs.DiceHolder()
    for i in range(n_dice):
        # Load instances of Dice into the dice_holder
        dice_holder.add(rs.Dice(i))
    logging.info('Set up - Dice holder: {}'.format(dice_holder))
    return dice_holder


def get_possible_steal_tokens(point_holder_player_list, temp_player):
    output = [x.last_token() for x in point_holder_player_list]
    # Remove own token from player that can steal
    output[temp_player-1] = rs.PointToken(0)
    return output



def get_winner(point_holder_player_list):
    player_points = [x.get_points() for x in point_holder_player_list]
    if sum(player_points) == 0:
        logging.info('Nobody scored a point. Nobody wins!')
        return 'None'
    winner = [i for i, x in enumerate(player_points) if x == max(player_points)]
    if len(winner) != 1:
        logging.info('Some players are tied on most worms')
        logging.info('The player with the highest point token value wins')
        highest_player_tokens = [point_holder_player_list[i].get_highest() for i in winner]
        winner_index = highest_player_tokens.index(max(highest_player_tokens))
        return winner[winner_index]+1
    else:
        logging.info('One player has the most worms')
        return winner[0]+1
