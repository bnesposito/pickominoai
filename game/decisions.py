import resources as rs
import numpy as np
import random
import logging


def pick_highest_points_dice(grouped_dice):
    temp_list = [i[2] for i in grouped_dice]
    output = [i for i, x in enumerate(temp_list) if x == max(temp_list)]
    if len(output) != 1:
        return max(output)
    else:
        return output[0]


def pick_dice_at_random(grouped_dice):
    n = len(grouped_dice)
    return random.randint(0, n-1)


def fix_illegal_dice_addition(selected_dice_holder, grouped_dice):
    flag_legal_add = 0
    temp_selected_dice_array = np.nan
    for i in range(len(grouped_dice)):
        # Stop iterating if there is a legal dice addition
        if flag_legal_add == 1:
            break
        # Try to fix the dice addition
        temp_selected_dice_index = len(grouped_dice) - 1 - i
        logging.warning('Illegal dice add detected')
        logging.info('Trying to fix {} with index {}'.format(i + 1, temp_selected_dice_index))
        temp_selected_dice_array = grouped_dice[temp_selected_dice_index]
        flag_legal_add = selected_dice_holder.check_legal_add(temp_selected_dice_array)
    return temp_selected_dice_array, flag_legal_add


def pick_highest_points_token(dice_holder_player_value, point_holder_board):
    temp_value = dice_holder_player_value
    highest_token = rs.PointToken(temp_value)
    flag_value_lower_than_board = 0
    flag_highest_token_in_set = 0
    while flag_highest_token_in_set == 0:
        if highest_token in point_holder_board:
            flag_highest_token_in_set = 1
        else:
            temp_value -= 1
            highest_token = rs.PointToken(temp_value)

        # Closure condition for cases where the value of the dice holder is smaller than the lowest PointToken in board
        if temp_value < 20:
            logging.warning('Value of dice is not enough to get a Point Token on Board')
            flag_value_lower_than_board = 1
            flag_highest_token_in_set = 1
    return flag_value_lower_than_board, highest_token


def player_stops_playing_at_random():
    return random.randint(0, 1)
